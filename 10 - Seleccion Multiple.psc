Algoritmo SeleccionMultiple
	nota = 0
	Escribir "Mete la nota"
	Leer nota
	Si nota =0 && nota<5 Entonces
		Escribir "Suspenso"
	Fin Si
	Si nota>=5 && nota<6 Entonces
		Escribir "Suficiente"
	Fin Si
	Si nota>=6 && nota<7 Entonces
		Escribir "Bien"
	Fin Si
	Si nota>=7 && nota <9 Entonces
		Escribir "Notable"
	Fin Si
	Si nota>=9 && nota<=10 Entonces
		Escribir "Sobresaliente"
	Fin Si
	Si nota<0 || nota>10 Entonces
		Escribir "El valor tiene que ser entre 0 y 10"
	FinSi
FinAlgoritmo
