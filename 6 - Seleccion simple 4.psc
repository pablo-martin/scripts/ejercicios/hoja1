Algoritmo SeleccionSimple4
	
	numero1 <- 0
	numero2 <- 0
	numero3 <- 0
	Escribir 'Introduce un numero'
	Leer numero1
	Escribir 'Introduce un numero'
	Leer numero2
	Escribir 'Introduce un numero'
	Leer numero3

	Si numero1<numero2 && numero1<numero3 Entonces
		Escribir "El menor es el ", numero1
	SiNo
		Si numero1==numero2 && numero1==numero3 Entonces
			Escribir "Son iguales"
		SiNo
			Si numero2<numero3 && numero2<numero1 Entonces
				Escribir "El menor es el ", numero2
			SiNo
				Escribir "El menor es el ", numero3
			Fin Si
		Fin Si
	Fin Si
FinAlgoritmo
